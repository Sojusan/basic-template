from setuptools import setup
from basic_template import __version__
from os import path


def get_version():
    return '.'.join(map(str, __version__))


with open('README.md', 'r') as fh:
    long_description = fh.read()


APP_NAME = 'basic_template'
DESCRIPTION = 'Basic app template.'

CONFIG = {
    'name': APP_NAME,
    'version': get_version(),
    'description': DESCRIPTION,
    'long_description': long_description,
    'long_description_content_type': 'text/markdown',
    'packages': [APP_NAME, 'requirements'],
    'package_data': {'requirements': ['requirements/*']},
    'include_package_data': True,
    'author': 'Sojusan',
    'author_email': 'change_me@email.com',
    'url': 'https://gitlab.com/Sojusan/basic-template',
    'license': 'proprietary',
    'classifiers': [
        'Programming Language :: Python :: 3',
        'License :: Other/Proprietary License',
    ],
    'scripts': [
        path.join('scripts', 'basic-template-manage.py'),
        path.join('scripts', 'basic-template-run-tests.sh'),
    ]
}

setup(**CONFIG)
