from django.contrib import admin
from django.urls import path, include
from django.utils import translation
from django.conf import settings

from rest_framework import permissions
from rest_framework.documentation import include_docs_urls

from basic_template.basic_template.generators import CustomOpenAPISchemaGenerator


urlpatterns = [
    path('admin/', admin.site.urls),
]


def force_language(get, language=settings.LANGUAGE_CODE):
    '''Force to use a certain language in get method response.

    Args:
        get (function): drf_yasg.views.SchemaView's method get with arguments: self, request,
            version and format.
        language (string, optional): Language code for drf-yasg documentation. Defaults to
            settings.LANGUAGE_CODE.
    '''
    def activate_language(self, request, version='', format_=None):
        translation.activate(language)
        return get(self, request, version, format_)

    return activate_language


if settings.DOCUMENTATION_ENABLED:
    # API documentation for developers
    title = 'Basic Template API'
    description = ('Basic Template API.')
    urlpatterns.append(path('', include_docs_urls(
        title=title,
        description=description,
        public=True,
        permission_classes=(permissions.AllowAny,),
        patterns=urlpatterns
    )))

    from drf_yasg.views import get_schema_view
    from drf_yasg import openapi

    schema_view = get_schema_view(
        openapi.Info(
            title=title,
            default_version='',
            description=description,
        ),
        public=True,
        permission_classes=(permissions.AllowAny,),
        patterns=urlpatterns,
        generator_class=CustomOpenAPISchemaGenerator
    )

    # Force documentation language to `en`.
    get_method = schema_view.get
    schema_view.get = force_language(get_method, 'en')

    urlpatterns += [
        path(
            'swagger/',
            schema_view.with_ui('swagger', cache_timeout=0),
            name='schema-swagger-ui'
        ),
        path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    ]

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [
        path('rosetta/', include('rosetta.urls'))
    ]
