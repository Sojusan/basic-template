from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from django.conf import settings


DEFAULT_PAGE = 1
DEFAULT_PAGE_SIZE = settings.REST_FRAMEWORK["PAGE_SIZE"]
MAX_PAGE_SIZE = settings.REST_FRAMEWORK["MAX_PAGE_SIZE"]


class CustomPagination(PageNumberPagination):
    page = DEFAULT_PAGE
    page_size = DEFAULT_PAGE_SIZE
    page_size_query_param = "page_size"
    max_page_size = MAX_PAGE_SIZE

    def get_paginated_response(self, data):
        return Response({
            "next": self.get_next_link(),
            "previous": self.get_previous_link(),
            "count": self.page.paginator.count,
            "page": int(self.request.GET.get("page", DEFAULT_PAGE)),
            "page_size": self._restrict_page_number(
                int(self.request.GET.get("page_size", self.page_size))
            ),
            "results": data
        })

    def _restrict_page_number(self, page_size):
        return max(min(page_size, self.max_page_size), 1)
