from basic_template.settings.base import * # noqa
import os

oeg = os.environ.get

DEBUG = oeg("DJANGO_DEBUG", "False").lower() == "true"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "db.sqlite3",
    }
}
