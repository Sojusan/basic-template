import os

from django.utils.translation import gettext_lazy as _

oeg = os.environ.get

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PARENT_BASE_DIR = os.path.dirname(BASE_DIR)

SECRET_KEY = oeg('DJANGO_SECRET_KEY', 'change_me')

DEBUG = oeg('DJANGO_DEBUG', 'False').lower() == 'true'

ALLOWED_HOSTS = oeg('DJANGO_ALLOWED_HOSTS', '*')

DOCUMENTATION_ENABLED = oeg('DJANGO_DOCUMENTATION_ENABLED', 'True').lower() == 'true'

MAIN_APP = 'basic_template'

INSTALLED_APPS = [
    # Django apps
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Third party apps
    'rest_framework',
    'drf_yasg',
    'django_extensions',
    'django_filters',

    # Internal apps
    f'{MAIN_APP}.{MAIN_APP}'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = f'{MAIN_APP}.{MAIN_APP}.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = f'{MAIN_APP}.{MAIN_APP}.wsgi.application'

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "db.sqlite3",
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGES = (
    ('pl', _('Polish')),
    ('en', _('English')),
)

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Warsaw'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = f'/opt/{MAIN_APP}/static'

MEDIA_URL = '/media/'
MEDIA_ROOT = f'/opt/{MAIN_APP}/media'

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'basic_template.basic_template.paginator.CustomPagination',
    'PAGE_SIZE': 50,
    'MAX_PAGE_SIZE': 100,
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend'
    ),
}

SWAGGER_SETTINGS = {
    'DEFAULT_MODEL_RENDERING': 'example',
}
