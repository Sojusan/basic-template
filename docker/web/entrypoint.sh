#!/bin/sh

case "$1" in
    gunicorn)
        gunicorn -c gunicorn.conf.py basic_template.basic_template.wsgi:application
    ;;
    set_state)
        basic-template-manage.py collectstatic --no-input && \
        basic-template-manage.py migrate --no-input
    ;;
    *)
        bash -c "$@"
    ;;
esac
