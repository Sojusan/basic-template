#!/bin/sh

# Get directory where this script lives, needed for running other scripts.
BASE_DIR="$(realpath $(dirname $0))"

pytest_args=""
if [ -n "$WITH_COVERAGE" ]
then
    pytest_args="$pytest_args --cov=basic_template"
    echo "With checking code coverage."
fi

echo "Running pytest."
# Eval is required here so that extra_pytest_args is properly
# expanded into arguments.
eval pytest $pytest_args "$@"
out=$?
if [ $out -ne 0 ]
then
    echo "Pytest failed!"
    exit $out
fi
echo "Pytest finished!"

if [ -z "$NO_FLAKE8" ]
then
    echo "Running flake8."
    flake8
    out=$?
    if [ $out -ne 0 ]
    then
        echo "Flake8 failed!"
        exit $out
    else
        echo "Flake8 finished!"
    fi
fi
