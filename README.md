# Basic Template

[![pipeline status](https://gitlab.com/Sojusan/basic-template/badges/master/pipeline.svg)](https://gitlab.com/Sojusan/basic-template/commits/master)
[![coverage report](https://gitlab.com/Sojusan/basic-template/badges/master/coverage.svg)](https://gitlab.com/Sojusan/basic-template/commits/master)

## About

Basic Django template application.

* Python 3.9.2
* Django 3.1.7
* Line length 100
* Language: English
* Full compatibility with PEP8
* Tests using PyTest (scripts: `basic-template-run-tests.sh`)
* Nginx
* Dockerization of the project
* Assumed coverage in tests > 90%

## Assumptions about the release preparation and deploy methodology

1. I keep 2 branches:
    * **master** - intended for code that has to run in production.
    * **devel** - containing the latest code, target for new developer code.
1. I use versioning in accordance with [semver 2.0](https://semver.org/spec/v2.0.0.html).
1. The `master`, `devel` branches should contain stable, working code.
1. Workflow:
    * The developer is preparing a branch with a new functionality or an correction, giving it a name as *[bugfix/feature/hotfix]-[description]*, e.g. *feature-red_button*
    * When the code is ready, the developer prepares MR (Merge Request) for the **devel** branch for new functionalities.
    * If all functionalities work, the code is uploaded to the **master** branch

    Tag Legend:

    * **latest** - The latest production version.
    * **1.0.0** - Production version.

## Deploy

1. Create the file `docker-compose.yaml` and upload it to the production server. (only the first time)
1. Log in via ssh to the production server and execute the following commands:

```shell
docker-compose -f docker-compose.yml pull
docker-compose -f docker-compose.yml up --detach --force-recreate --remove-orphans
```

## Local installation

1. Clone the repository from GitLab with the command:

    ```shell
    https://gitlab.com/Sojusan/basic-template.git
    ```

1. Create a virtual environment with [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/):

    ```shell
    mkvirtualenv -p python3.9.2 basic-template
    ```

1. Activate the virtual environment:

    ```shell
    workon basic-template
    ```

1. Update the pip package:

    ```shell
    pip install -U pip
    ```

1. Install all project dependencies:

    ```shell
    pip install -r requirements/requirements.txt
    ```

1. In the project root directory, execute the command:

    ```shell
    pip install -e .
    ```

1. Create a database:

    ```shell
    basic-template-manage.py migrate
    ```

1. Create a django admin account:

    ```shell
    basic-template-manage.py createsuperuser
    ```

1. Start the development server:

    ```shell
    basic-template-manage.py runserver
    ```

## Configuration

* All configuration can be found in `basic-template/settings/`
* **base.py** - contains the base configuration of the application
* **devel.py** - includes development configuration
* **test.py** - contains configuration for tests
* You can set up any configuration file using the `DJANGO_SETTINGS_MODULE` environment variable

## Tests

1. Running tests using the SQLite database:

    ```shell
    basic-template-run-tests.sh
    ```

2. Starting the full test stack with the tox utility:

    ```shell
    tox
    ```
