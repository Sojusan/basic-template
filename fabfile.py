import json

from distutils.util import strtobool
from fabric.api import local, prompt
from pathlib import Path


DOCKER_REGISTRY = 'registry.gitlab.com'
DOCKER_REGISTRY_PATH_WEB = f'{DOCKER_REGISTRY}/sojusan/basic-template/web'
DOCKER_REGISTRY_PATH_NGINX = f'{DOCKER_REGISTRY}/sojusan/basic-template/nginx'


def setup_project(path='dist'):
    '''
    Local - Create whl file for project.

    Args:
        path (str, optional): Path where .whl will be distributed. Defaults to 'dist'.
    '''
    local(f'python setup.py bdist_wheel --dist-dir={path}')


def clean_up(dist_path='dist'):
    '''
    Local - Cleaning after build project.

    Args:
        dist_path (str, optional): Path where .whl was distributed. Value set to None or 'None'
        prevents to remove dist directory. Defaults to 'dist'.
    '''
    local('python setup.py clean --all')
    local(f'rm -rf build/ *.egg-info/ {"" if not dist_path or dist_path == "None" else dist_path}')


def remove_whls(path='dist/'):
    '''
    Local - remove all whl files.

    Args:
        path (str, optional): Path to whl files. Defaults to 'dist/'.
    '''
    local(f'rm -f {path}*.whl')


def build_web(tag='latest', clean='true'):
    '''
    Local - build a web application container.

    Args:
        tag (str, optional): Tag for a builded web application container. Defaults to 'latest'.
        clean (str, optional): Cleaning after building. Defaults to 'true'.
    '''
    web_directory = 'docker/web/'
    clean_up(None)
    remove_whls(web_directory)
    setup_project(web_directory)
    local(f'cd {web_directory} && docker build . -t {DOCKER_REGISTRY_PATH_WEB}:{tag}')
    try:
        if strtobool(clean):
            clean_up(None)
            remove_whls(web_directory)
    except ValueError:
        raise ValueError("Wrong passed parameter for clean. Cleaning won't be executed.")


def build_nginx(tag='latest'):
    '''
    Local - build a nginx container.

    Args:
        tag (str, optional): Tag for a builded nginx container. Defaults to 'latest'.
    '''
    nginx_directory = 'docker/nginx'
    local(f'cd {nginx_directory} && docker build . -t {DOCKER_REGISTRY_PATH_NGINX}:{tag}')


def build_images(tag='latest'):
    '''
    Local - build all containers for project(web, nginx).

    Args:
        tag (str, optional): Tag for builded containers. Defaults to 'latest'.
    '''
    build_web(tag)
    build_nginx(tag)


def push_web(tag='latest'):
    '''
    Local - push a docker web container to registry.

    Args:
        tag (str, optional): Tag for a pushed web container. Defaults to 'latest'.
    '''
    _login_to_docker_registry()
    local(f'docker push {DOCKER_REGISTRY_PATH_WEB}:{tag}')


def push_nginx(tag='latest'):
    '''
    Local - push a docker nginx container to registry.

    Args:
        tag (str, optional): Tag for a pushed nginx container. Defaults to 'latest'.
    '''
    _login_to_docker_registry()
    local(f'docker push {DOCKER_REGISTRY_PATH_NGINX}:{tag}')


def push_images(tag='latest'):
    '''
    Local - push all builded containers(web, nginx) to registry.

    Args:
        tag (str, optional): Tag for builded containers. Defaults to 'latest'.
    '''
    push_web(tag)
    push_nginx(tag)


def docker_up():
    '''Local - Docker-compose up with deafult configuration.'''
    local('docker-compose -f docker/docker-compose.yml up')


def docker_build_and_up():
    '''Local - Build all images and docker-compose up with deafult configuration.'''
    build_images()
    docker_up()


def _is_docker_registry_access():
    '''Check if user is logged to docker registry'''
    try:
        path = '{}/.docker/config.json'.format(Path.home())
        with open(path, 'r') as config_file:
            return json.load(config_file)['auths'].get(DOCKER_REGISTRY) is not None
    except FileNotFoundError:
        return False


def _login_to_docker_registry():
    '''Login to docker registry if required'''
    if not _is_docker_registry_access():
        answer = prompt(
            (
                'Access to docker registry is required, '
                f'do you want to login to: {DOCKER_REGISTRY} [y/n]?'
            ),
            default='y'
        )
        if answer == 'y':
            local(f'docker login {DOCKER_REGISTRY}')
        else:
            print("Exiting; can't continue without login to docker registry.")
            exit(1)
